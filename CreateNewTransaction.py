import jwt
import json
import time
import math
import secrets
from hashlib import sha256
import requests
base_url = "https://api.fireblocks.io"
apiKey='5980f0db-59fd-5ed9-958d-46c97639fdf0'
walletId = '76e3f4a6-0c2c-bbe4-ffc9-ea4204beca62'
path = f"/v1/external_wallets/{walletId}"
private_key = open('fireblocks_secret.key', 'r').read()
timestamp = time.time()
timestamp_secs = math.floor(timestamp)
nonce = secrets.randbits(63)
data = {
}
hash = sha256(json.dumps(data).encode("utf-8")).hexdigest()
token = jwt.encode ({ 
    "uri": path,
    "nonce": nonce,
    "iat": timestamp_secs,
    "exp": timestamp_secs + 30, 
    "sub":apiKey,
    "bodyHash": hash, }, private_key, algorithm="RS256").decode('utf-8')
headers = { 
     "X-API-Key": apiKey, 
        "authorization": f'Bearer {token}',
      "Content-Type":"application/json"
     }

r = requests.request("GET", base_url + path, data=json.dumps(data)  ,headers=headers)
print(r)
print(r.text)